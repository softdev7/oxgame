/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxgame;
import java.util.*;
/**
 *
 * @author Windows10
 */
public class welcomeOX {
    static String XO[][] = {{"-","-","-"},
                            {"-","-","-"},
                            {"-","-","-"}};
	
    static String turn = "X";
    static int R, C;
    static int Round = 0;
    public static void main(String[] args) {
        //loop start
        System.out.println("Welcome to OX Game");
        for(;;){
            BoardGame();
            inputRowAndCol();
            if( checkWinner()) {
		break ;
            }
            else if (checkDraw()) {
		break ;
            }
            if (turn == "X") {
		turn = "O";
            } else {
		turn = "X";
            }
        }
    }
    public static void BoardGame() {
        
	System.out.println(" " + " 1 " + "2" + " 3 ");
            for (int i = 0; i < 3; i++) {
		System.out.print(i + 1);
                    for (int j = 0; j < 3; j++) {
                        System.out.print(" ");
			System.out.print(XO[i][j]);
                    }
		System.out.println();
		}
	}
    public static void inputRowAndCol() {
	Scanner kb = new Scanner(System.in);

            for (;;) {
		System.out.println(turn + " turn");
		System.out.println("Please input Row Col:");
                    String a = kb.next();
                    String b = kb.next();
                    R = Integer.parseInt(a);
                    C = Integer.parseInt(b);
                    R = R - 1;
                    C = C - 1;
                    Round++;
                    XO[R][C] = turn;
                    break;

            }
    }
    public static boolean checkWinner() {
	boolean win = false;
            for (int i = 0; i < XO.length; i++) {
		if (XO[i][0] == turn && XO[i][1] == turn && XO[i][2] == turn) {
                    win = true;
                }
            }
            for (int i = 0; i < XO.length; i++) {
		if (XO[0][i] == turn && XO[1][i] == turn && XO[2][i] == turn) {
                    win = true;
		}
            }
            if (XO[0][0] == turn && XO[1][1] == turn && XO[2][2] == turn) {
		win = true;
            }
            if (XO[0][2] == turn && XO[1][1] == turn && XO[2][0] == turn) {
		win = true;
            }
            if (win == true) {
		BoardGame();
		System.out.println("Player "+turn + " Win...");
                System.out.print("Bye bye ...");
		return true ;
            }
		return false ;
            
    }
    public static boolean checkDraw() {
	if (Round == 9) {
            BoardGame();
            System.out.println("Draw");
            System.out.print("Bye bye ...");
            return true ;
	}
            return false ;
	}
}

